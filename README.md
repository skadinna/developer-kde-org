# KDE Developer website

## Running the website locally

Download the latest Hugo release (extended version) from [here](https://github.com/gohugoio/hugo/releases)
 and clone this repo.

Once you've cloned the site repo, from the repo root folder, run:

```
hugo server
```
